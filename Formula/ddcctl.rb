class Ddcctl < Formula
  desc "DDC monitor controls (brightness) for Mac OSX command-line"
  homepage "https://github.com/kfix/ddcctl"
  url "https://github.com/kfix/ddcctl/archive/126422a7354d297bc805b66d1d1390c05e0f23fc.tar.gz"
  version "126422a7354d297bc805b66d1d1390c05e0f23fc"
  sha256 "970c3d81b474d94a61cf67e136543e7b96e86c3825fac1f862211b82f85462a7"
  head "https://github.com/kfix/ddcctl.git"

  def install
    system "make"
    bin.install "ddcctl"
  end

  test do
    system "#{bin}/ddcctl"
  end
end
